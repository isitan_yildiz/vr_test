const VR = false; // For demo
const IPD_PRECISION = 0.01;
let ipd = 1;
let default_gamepad = null;
let gamepad_poll_time = 10;
let update_timestamp = 0;
let timeline = -1;
let scenario;
let player;
let sign;
let npcs = [];
let sign_dir;

let resetIpd = function resetIpd() {
	scene.activeCameras[0].position.x = -ipd;
	scene.activeCameras[1].position.x = ipd;
};

let createScene = function createScene() {
	const scene = new BABYLON.Scene(engine);
	
	if (VR) {
		const camera_left = new BABYLON.UniversalCamera("camera_left", new BABYLON.Vector3(-ipd,5,-6),scene);
		const camera_right = new BABYLON.UniversalCamera("camera_right", new BABYLON.Vector3(ipd,5,-6),scene);
		camera_left.setTarget(BABYLON.Vector3.Zero());
		camera_right.setTarget(BABYLON.Vector3.Zero());
		camera_left.viewport = new BABYLON.Viewport(0,0,0.5,1);
		camera_right.viewport = new BABYLON.Viewport(0.5,0,0.5,1);
		camera_left.attachPostProcess(new BABYLON.VRDistortionCorrectionPostProcess("left", camera_left, false, BABYLON.VRCameraMetrics.GetDefault()));
		camera_right.attachPostProcess(new BABYLON.VRDistortionCorrectionPostProcess("right", camera_right, true, BABYLON.VRCameraMetrics.GetDefault()));
		scene.activeCameras.push(camera_left);
		scene.activeCameras.push(camera_right);
		scene.onPointerDown = function() {
			scene.onPointerDown = undefined;
			camera_left.attachControl(canvas, true);
			camera_right.attachControl(canvas, true);
		};
		player = camera_left;
	} else {
		const camera = new BABYLON.UniversalCamera("camera", new BABYLON.Vector3(0,1,-6),scene);
		camera.setTarget(new BABYLON.Vector3(0,1,0));
		scene.onPointerDown = function() {
			scene.onPointerDown = undefined; 
			camera.attachControl(canvas, true);
		}
		player = camera;
	}

	const light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0,1,0),scene);
	
	skybox = BABYLON.MeshBuilder.CreateBox("skybox", {size:1024}, scene);
	const skyboxMaterial = new BABYLON.StandardMaterial("skybox",scene);
	skyboxMaterial.backFaceCulling = false;
	skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("assets/textures/skybox", scene);
	skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
	skyboxMaterial.diffuseColor = new BABYLON.Color3(0,0,0);
	skyboxMaterial.specularColor = new BABYLON.Color3(0,0,0);
	skybox.material = skyboxMaterial;
	
	
	// NPC 0
	BABYLON.SceneLoader.ImportMeshAsync(["Cube"], "assets/models/prod/", "lpcharacters1.babylon").then((result) => {
		npcs.push(result.meshes[0]);
		npcs[0].scaling = new BABYLON.Vector3(0.5,0.5,0.5);
		npcs[0].position = new BABYLON.Vector3(-3,3,300);
		npcs[0].rotation.y = -0.5;
	});
	// NPC1
	BABYLON.SceneLoader.ImportMeshAsync(["Cube.003"], "assets/models/prod/", "lpcharacters1.babylon").then((result) => {
		npcs.push(result.meshes[0]);
		npcs[1].scaling = new BABYLON.Vector3(0.5,0.5,0.5);
		npcs[1].position = new BABYLON.Vector3(2,3,330);
		npcs[1].rotation.y = 0.30;
	});
	// Tree
	BABYLON.SceneLoader.ImportMeshAsync(["pine_00_Cylinder.001"], "assets/models/prod/", "pines.babylon").then((result) => {
		// Create forest
		let mesh = result.meshes[0];
		mesh.position = new BABYLON.Vector3(-10,0,200);
		//mesh.isVisible = false;
		let seperation = 10;
		let randomness = 4;
		let x;
		for (let i = 0; i < 5; i++) {
			x = mesh.position.x - i*seperation;
			for (let j = 0; j < 20; j++) {
				let instance = mesh.createInstance("tree_" + i);
				instance.position.x = x - randomness + 2*randomness*Math.random();
				instance.position.y = mesh.position.y;
				instance.position.z = mesh.position.z - j * seperation - randomness + 2*randomness*Math.random();
				instance.rotation.y = -Math.PI + 2*Math.PI*Math.random();
			}
		}
	});
	// Sign
	BABYLON.SceneLoader.ImportMeshAsync(["sign_00"], "assets/models/prod/", "road assets.babylon").then((result) => {
		sign_dir = Math.floor(2*Math.random());
		sign = result.meshes[0];
		if (sign_dir) {
			sign.rotation.y = Math.PI;
			sign.position = new BABYLON.Vector3(11,-1,100);
		} else {
			sign.position = new BABYLON.Vector3(-4,-1,100);
		}
	});
	// Wheelbarrow
	BABYLON.SceneLoader.ImportMeshAsync(["Cube"], "assets/models/prod/", "road assets.babylon").then((result) => {
		result.meshes[0].position = new BABYLON.Vector3(-4,0,305);
	});
	// House 0
	BABYLON.SceneLoader.ImportMeshAsync(["house.000"], "assets/models/prod/", "houses.babylon").then((result) => {
		result.meshes[0].scaling = new BABYLON.Vector3(2,2,2);
		result.meshes[0].position = new BABYLON.Vector3(-8,2,275);
	});
	// House 1
	BABYLON.SceneLoader.ImportMeshAsync(["house.001"], "assets/models/prod/", "houses.babylon").then((result) => {
		result.meshes[0].scaling = new BABYLON.Vector3(2,2,2);
		result.meshes[0].position = new BABYLON.Vector3(17,2,275);
	});
	
	return scene;
};

let increaseIpd = function increaseIpd() {
	ipd += IPD_PRECISION;
	if (ipd > 2) {
		ipd = 2;
	} else {
		resetIpd();
	}
};

let decreaseIpd = function decreaseIpd() {
	ipd -= IPD_PRECISION;
	if (ipd < 0) {
		ipd = 0;
	} else {
		resetIpd();
	}
};

let gamepadControl = function gamepadControl() {
	if (default_gamepad && default_gamepad.timestamp > update_timestamp + gamepad_poll_time) {
		update_timestamp = default_gamepad.timestamp;
		let buttons = default_gamepad.buttons.map((value) => {
			return value.pressed;
		});
		console.log(buttons);
		console.log(default_gamepad.axes);
		if (buttons[0]) {
			increaseIpd();
		} else if (buttons[1]) {
			decreaseIpd();
		}
	}
};

let distanceSquare = function distanceSquare(camera, object) {
	return (camera.position.x-object.position.x)**2 + (camera.position.z-object.position.z)**2;
}

let indexToLetter = function indexToLetter(index) {
	return String.fromCharCode(65+index);
}

let question = function question() {
	let choice = Math.floor(scenario.questions[timeline].length*Math.random());
	while(true) {
		let text = scenario.questions[timeline][choice].question + '\n';
		let answer = Math.floor((scenario.questions[timeline][choice].options.length+1)*Math.random());
		for (let i = 0; i < scenario.questions[timeline][choice].options.length+1; i++) {
			text += indexToLetter(i) + '. ';
			if (i < answer) {
				text += scenario.questions[timeline][choice].options[i] + '\n';
			} else if (i > answer) {
				text += scenario.questions[timeline][choice].options[i-1] + '\n';
			} else {
				text += scenario.questions[timeline][choice].answer + '\n';
			}
		}
		text += "Please choose A, B, C or D.";
		let input = window.prompt(text,"");
		if (indexToLetter(answer) === input.match(/[A-Z]/i)[0].toUpperCase()) {
			window.alert("Correct!");
			timeline++;
			break;
		} else {
			window.alert("Sorry, wrong answer. Try again.");
		}
	}
}

let colCheck = function colCheck() {
	if (timeline === -1) {
		if (sign && distanceSquare(player, sign) < 400) {
			if (sign_dir) {
				window.alert("Sign: EAST");
			} else {
				window.alert("Sign: WEST");
			}
			timeline++;
		}
	} else if (timeline === 2) {
		window.alert("Scenario Complete!\nLooping for demo...");
		timeline++;
		window.setTimeout(function(){
			window.location.reload();
		}, 1000);
	} else {
		if (npcs[timeline] && distanceSquare(player, npcs[timeline]) < 225) {
			question();
		}
	}
}

window.onload = function() {
	canvas = document.getElementById("renderCanvas");
	engine = new BABYLON.Engine(canvas, true);
	scene = createScene();

	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (this.readyState === 4 && this.status >= 200 && this.status < 300) {
			scenario = JSON.parse(xhr.responseText);
		}
	};
	xhr.open("GET","assets/demo_stage/lines.json",true);
	xhr.send();

	engine.runRenderLoop(function() {
		gamepadControl();
		if (!VR) {
			if (scene.activeCamera.position.y !== 1) {
				scene.activeCamera.position.y = 1;
			}
		}
		colCheck();
		scene.render();
	});

	window.addEventListener("resize", function() {
		engine.resize();
	});
	
	canvas.addEventListener("keypress", function(event) {
		let key = event.keyCode;
		switch(key){
			case 119:
				increaseIpd();
				break;
			case 115:
				decreaseIpd();
		}
	});
	
	window.addEventListener("gamepadconnected", (event) => {
		default_gamepad = event.gamepad;
	});
	
	
};
